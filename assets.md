# Assets

This file is here to document the source of the assets used in this game.

## Images

* Coffee shop - https://pixabay.com/photos/cafe-starbucks-coffee-restaurant-266651/
* view of the city - https://unsplash.com/photos/VW8MUbHyxCU
* Detective Office - https://pixabay.com/photos/vintage-office-typewriter-4018107/
* Man - https://pixabay.com/photos/man-male-portrait-beard-stylish-1122364/

## Audio