﻿# The script of the game goes in this file.

# Declare characters used by this game. The color argument colorizes the
# name of the character.

define em = Character('Eric Manner', color="#00700b")
define m = Character('Me', color="#ffffff")
define ic = Character('Officer Singham', color="#0d0057")
define mg = Character('Martin Gregg', color="#e6ff00")
define kl = Character('Kathy Lynn', color="#eaff00")
define df = Character('David Francis', color="#ff0000")
define ld = Character('Leonard Dexter', color="#eaff00")


# The game starts here.

label start:

    # Show a background. This uses a placeholder by default, but you can
    # add a file (named either "bg room.png" or "bg room.jpg") to the
    # images directory to show it.

    scene city morning

    # These display lines of dialogue.
    
    "Wednesdays are the worst day of the week for a private eye. The optomism of the beginning of the week is gone and the hope of weekend seems like a ways off."
    
    "This Wednesday was worse than most because the rent was a week past due. It didn't help that I hadn't seen a client in over a month. The landlord was trying to give me the hint by showing my office to potential renters. He even refered to me as \"That deadbeat who'll be out on his ear soon\"."
    
    "After the third potenetial tenant left, I decided to go to lunch and see how things went after that. "

    scene cafe
    with dissolve

    "I headed to a coffeeshop just down the bloack from me. I had once helped the owner out of a jam and he gave me my lunches for helf prive ever since then." 

    "I ordered a chicken club sandwich and two pots of coffee. As I ate, I watched the mass of humanity that entered the coffeeshop and pasted on the street. Unfortunately, none of them look like they werein need of a detective."

    "I was just about to head back to my empty office when I was approached by a man in bad need of a hair cut."

    # Eric Manner enters the story and the screen here.

    show eric manner

    em "Mr. Spinner? Your landlord told me I might find you here. He was pretty insistant I come looking for you."

    "I looked him up and down. \"He's only mad because I'm trying to live there rent free.\" Come on up to the office. It's more private."

    scene office
    with dissolve

    show eric manner

    "Back in my office, my client gave his new surroundings an uneasy once over."

    m "It doesn't look like much, but the rent is very low. Please have a seat and tell me what I can do for you."

    "He took a seat on one of the less rickety-looking chairs. After taking a deep breath, he start talking."

    em "Mr. Spinner, my name is Eric Manner. I'm a computer science major at CityTech. I'm majoring in game design. In fact, I like to write video games in my spare time."

    "I noddeed to show him I was listening."

    em "Recently, one of the largest game companies in the country, Dexter Studios, announced a contest for college students. Whoever creates the best game will get a hefty contract."

    m "How much are we talking here?"

    em "About $250,000."

    "I whistled. That was quite a hunck of change."

    m "So what brings you to my door?"

    em ""

    # This ends the game.

    return